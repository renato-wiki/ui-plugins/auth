/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/* eslint no-sync:off */
"use strict";

const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");
const serveStatic = require("serve-static");

const fsReadFile = Promise.promisify(fs.readFile);

const BASE_DIR = path.join(__dirname, "..");
const SCRIPTS_DIR = path.join(BASE_DIR, "target", "scripts");
const TEMPLATES_DIR = path.join(BASE_DIR, "templates");
const SNIPPET_NAVIGATION = fs.readFileSync(path.join(TEMPLATES_DIR, "snippets", "navigation.pug"));

/*===================================================== Exports  =====================================================*/

module.exports = Plugin;

/*==================================================== Functions  ====================================================*/

function Plugin(renato) {
  this.renato = renato;
}

Plugin.prototype.init = function () {
  let authPlugin = this.renato.pluginByRole.AUTH;
  if (authPlugin == null) { throw new Error("Auth plugin not found."); }
  this.authInstance = authPlugin.instance;
  this.auth = this.authInstance.middleware;
};

Plugin.prototype.bindRoutes = function (routers) {
  let usersMiddleware = [this.auth.role("admin")];
  let usersResolver = routers.route("users", "/users", this.usersLocals.bind(this), ...usersMiddleware);
  if (routers.ui != null) {
    let serveOptions = {cacheControl: !this.renato.debug, index: false};
    routers.ui.use("/scripts/plugins/ui-plugin-auth", serveStatic(SCRIPTS_DIR, serveOptions));
    routers.ui.get("/login", this.auth.guest(), this.routeLogin.bind(this));
    routers.ui.post("/users", ...usersMiddleware, this.createUser.bind(this, false), usersResolver.ui);
    routers.ui.delete("/users/:id", ...usersMiddleware, this.removeUser.bind(this));
  }
  if (routers.api != null) {
    routers.api.get("/login", this.auth.guest(), (req, res) => res.send());
    routers.api.post("/users", ...usersMiddleware, this.createUser.bind(this, true));
    routers.api.delete("/users/:id", ...usersMiddleware, this.removeUser.bind(this));
  }
};

Plugin.prototype.addNavigation = function (data) {
  if (data.content != null && data.content.pug != null) { data.content.pug += SNIPPET_NAVIGATION; }
  return data;
};

Plugin.prototype.readViewPre = function (view, data) { /* {view, options, content} */
  data.file = path.join(TEMPLATES_DIR, "views", view + ".pug");
  return data;
};

Plugin.prototype.readView = function (view, data) { /* {view, options, content} */
  return fsReadFile(data.file)
      .then((pugSource) => {
        data.content.pug = pugSource;
        return data;
      });
};

Plugin.prototype.routeLogin = function (req, res, next) {
  this.renato
      .hookReduce("theme::view:render", {view: "login", locals: {}, req, res})
      .catch(next);
};

Plugin.prototype.usersLocals = function () { return {users: _.map(this.authInstance.users.list, prepareUser)}; };

Plugin.prototype.createUser = function (returnUser, req, res, next) {
  this.authInstance
      .addUser(req.body.username, req.body.password, [])
      .then((user) => {
        this.authInstance.persist(); // runs async in background
        if (returnUser) { return res.send(prepareUser(user)); }
        next();
      }, next);
};

Plugin.prototype.removeUser = function (req, res, next) {
  if (this.authInstance.removeUser(req.params.id)) {
    this.authInstance.persist(); // runs async in background
    res.status(200).send();
  } else {
    next({status: 404, message: "User not found."});
  }
};

function prepareUser(user) { return _.omit(user, ["password"]); }
