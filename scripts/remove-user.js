/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
document.addEventListener("DOMContentLoaded", function () {
  "use strict";

  var removeUserBtnCheck = genClassNameRegex("remove-user-btn");

  document
      .getElementById("users-table")
      .addEventListener("click", onClick);

  function onClick(event) {
    var target = event.target;
    if (removeUserBtnCheck.test(target.className)) {
      var xhr = new XMLHttpRequest(),
          method = "DELETE",
          url = window.renato.rootPath + "users/" + target.getAttribute("data-user-id");
      xhr.open(method, url, true);
      xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          target.parentNode.parentNode.parentNode.remove();
        }
      };
      xhr.send();
    }
  }

  function genClassNameRegex(className) { return new RegExp("(?:^|\\s)" + className + "(?:\\s|$)"); }

});
